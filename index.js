db.courses.find({

	$and: [

		{instructor: {$regex: 'Sir Rome', $options: '$i'}},
		{price: {$gte: 20000}}
	]
})

db.courses.find(

	{instructor: {$regex: "Ma'am Tine", $options: '$i'}},
	{_id: 0,name: 1}
)

db.courses.find(

	{instructor: {$regex: "Ma'am Miah", $options: '$i'}},
	{_id: 0,name: 1, price: 1}
)

db.courses.find({

	$and: [

		{name: {$regex: 'r', $options: '$i'}},
		{price: {$gte: 20000}}
	]
})

db.courses.find({

	$and: [

		{isActive: true},
		{price: {$lte: 25000}}
	]
})